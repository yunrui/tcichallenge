# README #


### Poopcorn Time###
* https://poopcorntime.herokuapp.com/
* Read boring movie reviews when you poop...it's better than reading the shampoo bottle
* Nightly Built 
* used https://github.com/ahmetabdi/themoviedb gem
### Run Tests ###

* rake test test/controllers/movies_controller_test.rb
* rake test test/controllers/reviews_controller_test.rb
* rake test test/models/review_test.rb

### To Do ###
* Add more error messages for client side user input problems
* Improve user experience when add a new review
* More tests
* More lists of default movies on front page(upcoming, lastest, etc)
* reorganize javascripts (I prefer https://github.com/kbparagua/paloma)
### Known Issues ###
* Sort by Genre is slow because I could not find a genre key in the movies hash from the current API. Needed to manually go through the movies (20).
* Ajax to movies page was added 5:40pm. I tend to work with HTML then add javascript once its all working.(and I forgot to do)
* rake test test/controllers/movies_controller_test.rb has a typo and was failing. Fixed at 6:09pm.
* Search functionality search for all movies in the API's database. This can cause some confusion because the sorting/filter only sort/filter the exist list of movies (Popular List)
* Weird indentation in Movies Controller (should be 2 spaces)
* Lack of comments (Plan to refactor and comment)

### Design Decisions ###
* No movie model for simplicity (This result a a relative fat controller for movies, which is NOT good. Plan to move most of the controller code to model)
* Every review must have a movie id (movie title is option but by default fetch for better display in Read All Reviews)
* Additional Database constraint for reviews: [:movie_id,:user_email] so that each user can only rate one movie once


### Live ###
https://poopcorntime.herokuapp.com/