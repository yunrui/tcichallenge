require 'test_helper'

class ReviewTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'should require a valid user email' do
  	review = Review.new(:user_email=>"",:rating=>10,:movie_id=>1)
    assert_not  review.save
  	review = Review.new(:user_email=>"wrong format",:rating=>10,:movie_id=>1)
    assert_not review.save
  end

  test 'should require a rating' do
  	review = Review.new(:user_email=>"joedoe@.tcichanllenge.com",:rating=>nil,:movie_id=>1)
    assert_not review.save
  end

  test 'should require a movid id' do
    review = Review.new(:user_email=>"joedoe@.tcichanllenge.com",:rating=>10,:movie_id=>nil)
    assert_not review.save
  end

  test 'should create a new review with user email and rating' do
  	review = Review.new(:user_email=>"joedoe@.tcichanllenge.com",:rating=>10,:movie_id=>1)
    assert review.user_email=="joedoe@.tcichanllenge.com"
  end 

  test 'should not have the same email rate the same movie twice' do
    review1 = Review.new(:user_email=>"joedoe@.tcichanllenge.com",:rating=>10,:movie_id=>1)
    assert review1.user_email=="joedoe@.tcichanllenge.com"

    review2 = Review.new(:user_email=>"joedoe@.tcichanllenge.com",:rating=>9,:movie_id=>1)
    assert_not review2.save
  end  	
end
