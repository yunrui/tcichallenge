require 'test_helper'

class MoviesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "should get index and assign movies and genres" do
    get :index
    assert_response :success
    assert_not_nil assigns(:movies)
    assert_not_nil assigns(:genres)
  end

   test "should get show and assign a movie" do
    get :show,{:id=>1}
    assert_response :success
    assert_not_nil assigns(:movie)
  end
end
