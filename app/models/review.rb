class Review < ActiveRecord::Base
  validates :user_email, :presence => true
  validates_format_of :user_email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  validates :rating, :presence => true

  def self.get_average_for_movie(id)
  	reviews = Review.where(:movie_id=>id)
  	if reviews.size==0
  		return 0
  	else
  		values = reviews.collect{|x| x["rating"]}
  		puts values
  		return values.reduce(:+)/reviews.size
  	end 
  end
end
