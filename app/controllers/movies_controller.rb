
class MoviesController < ApplicationController

	def index
		Tmdb::Api.key("37f65bf08da3e2a621b7e23dbb2fe510")
		begin
			@movies = Tmdb::Movie.popular
			if params[:title]
				@search = Tmdb::Search.new
				@search.resource('movie') # determines type of resource
				@search.query(params[:title]) # the query to search against
				@result = @search.fetch # makes request
				puts @result.inspect
				@movies = @result
			else
				case params[:sort_by]
				when 'title'
					if params[:order]=='asc'
						@movies = @movies.sort_by { |x| x["title"] }
					else
						@movies = @movies.sort_by { |x| x["title"] }.reverse
					end
				when 'release_date'
					if params[:order]=='asc'
						@movies = @movies.sort_by { |x| x["release_date"] }
					else
						@movies = @movies.sort_by { |x| x["release_date"] }.reverse
					end
				when 'genre'
					@res_movies=[]
					@movies.each  do |movie|
						genres = Tmdb::Movie.detail(movie["id"])["genres"].collect{|x| x["name"]}.inspect
						if genres.include? params[:order]
							@res_movies<<movie
						end
					end
					@movies=@res_movies
				end
	  	end
	  rescue SocketError =>e
	  	puts e.inspect
	  	@movies=[]
		end
		
		begin
			@genres = Tmdb::Genre.list["genres"]
		rescue SocketError =>e
			puts e.inspect
			@genres=[]
		end
		respond_to do |format|
				format.js
        format.html 
    end
	end

	def show
		@movie=Tmdb::Movie.detail(params[:id])
		@reviews = Review.where(movie_id:params[:id])
	end
	
end
