class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :movie_id,null:false
      t.string :user_email,null:false
      t.integer :rating,null:false
      t.string :movie_title
      t.text :comment
      t.timestamps null: false
    end
    add_index :reviews, [:movie_id, :user_email], :unique => true
  end
end
